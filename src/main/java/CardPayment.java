import java.util.Scanner;

public class CardPayment {

    private int accountBalance;
    private int pin;
    private int inputPin;
    private String balanceMessage = "System message: Insufficient balance";
    private String transactionAcceptedMessage = "System message: Transaction accepted. Your new balance is ";

    public CardPayment(int accountBalance, int pin) {
        this.accountBalance = accountBalance;
        this.pin = pin;
    }

    public int getAccountBalance() {
        return accountBalance;
    }

    public void payCard(int amountToPay) {
        if (amountToPay > 50) {
            readPin();
            if(inputPin==pin) checkBalance(amountToPay);
        } else checkBalance(amountToPay);
    }


    public void readPin() {
        System.out.println("Please input PIN");
        for (int i = 0; i < 3; i++) {
            Scanner readPin = new Scanner(System.in);
            inputPin = readPin.nextInt();
            if (pin == inputPin) {
                System.out.println("Pin accepted");
                break;
            }
            if (i == 2 && pin != inputPin)
                System.out.println("Third attempt were incorrect. Card is blocked");
            else System.out.println("Pin is not accepted.Please try again");
        }
    }

    public void checkBalance(int amountToPay) {
        if (amountToPay <= accountBalance) {
            accountBalance = accountBalance - amountToPay;
            System.out.println(transactionAcceptedMessage + accountBalance);
        } else System.out.println(balanceMessage);
    }
}


