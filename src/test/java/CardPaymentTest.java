

public class CardPaymentTest {

    /*Testy bez asercji xd*/

    public static void main(String[] args) {
        System.out.println("Amount is lower than 50zl, insufficient amount of balance");
        CardPayment cardPayment = new CardPayment(20, 1234);
        cardPayment.payCard(40);


        CardPayment cardPayment2 = new CardPayment(200, 1234);
        System.out.println("\nAmount is lower than 50zl, amount of balance is sufficient - " + cardPayment2.getAccountBalance() + "zl");
        cardPayment2.payCard(40);


        /*Please put correct PIN*/
        CardPayment cardPayment3 = new CardPayment(200, 1234);
        System.out.println("\nAmount is greater than 50zl, amount of balance is sufficient - " + cardPayment3.getAccountBalance() + "zl and first/second/third attempt to enter the PIN is correct");
        cardPayment3.payCard(60);

        /*Please put correct PIN*/
        CardPayment cardPayment4 = new CardPayment(50, 1234);
        System.out.println("\nAmount is greater than 50zl, amount of balance is insufficient - " + cardPayment4.getAccountBalance() + "zl and first/second/third attempt to enter the PIN is correct");
        cardPayment4.payCard(60);

        /*Please put incorrect PIN*/
        CardPayment cardPayment5 = new CardPayment(50, 1234);
        System.out.println("\nAmount is greater than 50zl, amount of balance is insufficient - " + cardPayment5.getAccountBalance() + "zl and PIN is incorrect (3 attempts)");
        cardPayment5.payCard(60);
    }
}

